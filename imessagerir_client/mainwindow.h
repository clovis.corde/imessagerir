#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "login.h"
#include "messenger.h"
#include "client.h"

#include <QHostAddress>
#include <QMainWindow>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

private:
    Client mClient;
private slots:
    void switchScreenConv(QString username);

};

#endif // MAINWINDOW_H

