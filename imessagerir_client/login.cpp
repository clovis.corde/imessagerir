#include "login.h"
#include "ui_login.h"

#include <QDebug>
#include <QThread>

Login::Login(Client &client, QWidget *parent) :
    QWidget(parent)
    , mClient(client)
    , ui(new Ui::Login)
{
    ui->setupUi(this);

    ui->passwordLineEdit->setEchoMode(QLineEdit::Password);

    // Click on button "Login" to send a login request
    connect(ui->pbLogin, SIGNAL(clicked(bool)), this, SLOT(tryLogin()));
    // Press enter on the QLineEdit for password to send a login request
    connect(ui->passwordLineEdit, SIGNAL(editingFinished()), this, SLOT(tryLogin()));

    // Click on button "Connect" to send a connection request
    connect(ui->pbConnect,SIGNAL(clicked(bool)),this,SLOT(connectServer()));
    // Press enter on the QLineEdit for host to send a connection request
    connect(ui->portLineEdit, SIGNAL(editingFinished()), this, SLOT(connectServer()));

    connect(&mClient, SIGNAL(emitLogin()), this, SLOT(tryLogin()));
    // Display connected to the server on the screen
    connect(&mClient, SIGNAL(emitConnected()), this, SLOT(ackConnection()));

}

Login::~Login()
{
    delete ui;
}

void Login::tryLogin()
{
    qDebug() << "emit login";
    QString username = ui->usernameLineEdit->text();
    QString password = ui->passwordLineEdit->text();
    qDebug() << username << password;
    emit login(username, password);
}


void Login::connectServer()
{
    QString server = ui->serverLineEdit->text();
    QHostAddress addr = QHostAddress(server);
    quint16 port = ui->portLineEdit->text().toInt();
    emit connection(addr, port);
}

void Login::ackConnection()
{
    ui->labelConnect->setText("Connected, Login now");
}

void Login::ackLogin(bool valid)
{
    if(valid == false)
        ui->labelLogin->setText("Invalid login/password");
}
