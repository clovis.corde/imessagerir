#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QJsonObject>
#include <QJsonDocument>
#include <QHostAddress>

class Client : public QObject
{
    Q_OBJECT

private:
    QTcpSocket *mSocket;
    QString user;

public:
    Client(QObject *parent = nullptr);
    QString username(void);
    void login(QString username, QString password);

signals:
    void emitLogin();
    void emitSwitch(QString username);
    void emitConnected();
    void appendText(QString);

private slots:
    void ackConnection();
    void sendMessage(QString msg);
    void dataReceived();
    void msgConnection(QHostAddress addr, quint16 port);
    void sendLogin();
    void msgLogin(QString username, QString password);
};

#endif // CLIENT_H
