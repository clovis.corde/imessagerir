#ifndef CONVTAB_H
#define CONVTAB_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QTextBrowser>
#include <QVBoxLayout>
#include <QPrinter>
#include <QFileInfo>
#include <QFileDialog>

#include "client.h"

class ConvTab : public QWidget
{
    Q_OBJECT
public:
    ConvTab(Client &client, QWidget *parent = nullptr);

private:
    QLineEdit *mMsgLineEdit;
    QTextBrowser *mConvDisplay;
    Client &mClient;
signals:
    void sendMessage(QString msg);

private slots:
    void emitSendMessage();
    void appendMsg(QString msg);
    void exportAsPdf();

};

#endif // CONVTAB_H
