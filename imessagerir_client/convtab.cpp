#include "convtab.h"

#include <QDebug>

ConvTab::ConvTab(Client &client, QWidget *parent) : QWidget(parent) , mClient(client)
{
    mMsgLineEdit = new QLineEdit(this);
    QPushButton *pbSend = new QPushButton("Send",this);
    QPushButton *pbExport = new QPushButton("Export PDF",this);

    mConvDisplay = new QTextBrowser(this);

    QVBoxLayout *Vlayout = new QVBoxLayout(this);
    Vlayout->addWidget(mConvDisplay);

    QHBoxLayout *Hlayout = new QHBoxLayout(this);
    Hlayout->addWidget(mMsgLineEdit);
    Hlayout->addWidget(pbSend);
    Hlayout->addWidget(pbExport);

    Vlayout->addLayout(Hlayout);

    QString msg = mMsgLineEdit->text();
    connect(pbSend, SIGNAL(clicked(bool)), this, SLOT(emitSendMessage()));
    connect(pbExport, SIGNAL(clicked(bool)), this, SLOT(exportAsPdf()));

}

void ConvTab::emitSendMessage()
{
    qDebug() << "Signal sendMessage convtab";
    QString msg = mMsgLineEdit->text();
    emit sendMessage(msg);
    // Clear the message
    mMsgLineEdit->clear();
}

void ConvTab::appendMsg(QString msg)
{
    QString now = QDateTime::currentDateTime().toString("dd MM yyyy hh:mm");
    if(msg.size()!=0)
        mConvDisplay->append(now+" : -> " +msg);
}

void ConvTab::exportAsPdf()
{
    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    QString fileName = QFileDialog::getSaveFileName(this, "Export PDF", "", "*.pdf");
    if (QFileInfo(fileName).suffix().isEmpty())
        fileName.append(".pdf");
    printer.setOutputFileName(fileName);

    mConvDisplay->document()->print(&printer);
}
