#include "messenger.h"
#include "login.h"
#include "ui_messenger.h"

using namespace std;

Messenger::Messenger(Client &client, QWidget *parent)
    : QWidget(parent)
    , mClient(client)
    , ui(new Ui::Messenger)
{
    ui->setupUi(this);
    this->setMinimumSize(800,600);
    createWindowConv();

}

Messenger::~Messenger()
{
    delete ui;
}

// Create the QTabWidget for all the conversations
void Messenger::createWindowConv()
{
    qDebug() << "New MainWIndow";
    ui->myTabWidget->setTabPosition(QTabWidget::West);
    ui->myTabWidget->clear();

    //Create New Conversation
    connect(ui->pbNewConv,SIGNAL(clicked(bool)),this,SLOT(createPopUpConv()));
    connect(&mClient, SIGNAL(appendText(QString)), this, SLOT(emitDataReceived(QString)));

}

// PopUp to enter the username for the new conversation
void Messenger::createPopUpConv()
{
    QString username = QInputDialog::getText(this, tr("New Conversation"),
                                         tr("Username:"), QLineEdit::Normal,
                                         "Clovis");

    // Don't create the conversation if we clicked cancel or the username is empty
    if(username.size() != 0)
        createConv(username);
}

// Add a tab to the QTabWidget
void Messenger::createConv(QString username)
{
    ConvTab *tab = new ConvTab(mClient, this);

    ui->myTabWidget->addTab(tab,username);

    // Switch to the new conversation tab
    ui->myTabWidget->setCurrentIndex(ui->myTabWidget->indexOf(tab));
    connect(tab, SIGNAL(sendMessage(QString)),this,SLOT(sendMessage(QString)));
    connect(tab, SIGNAL(sendMessage(QString)), &mClient, SLOT(sendMessage(QString)));

    connect(tab, SIGNAL(sendMessage(QString)), tab, SLOT(appendMsg(QString)));

    emit usernameClient(username);
}

void Messenger::sendMessage(QString msg)
{
    qDebug() << "username" << mClient.username();
    emit sendMessageClient(mClient.username(),msg);
}

void Messenger::emitDataReceived(QString msg)
{
    emit appendDataReceived(msg);
}
