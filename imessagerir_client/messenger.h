#ifndef MESSENGER_H
#define MESSENGER_H

#include <QTcpSocket>
#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>

#include "convtab.h"
#include "client.h"

QT_BEGIN_NAMESPACE
namespace Ui { class Messenger; }
QT_END_NAMESPACE

class Messenger : public QWidget
{
    Q_OBJECT

public:
    Messenger(Client &client, QWidget *parent = nullptr);
    ~Messenger();

private:
    Client &mClient;
    Ui::Messenger *ui;

signals:
    void sendMessageClient(QString username, QString msg);
    void appendDataReceived(QString msg);
    void usernameClient(QString user);

private slots:
    void createWindowConv();
    void createConv(QString);
    void createPopUpConv();
    void sendMessage(QString msg);
    void emitDataReceived(QString msg);
};
#endif // MESSENGER_H
