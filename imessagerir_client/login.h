#ifndef LOGIN_H
#define LOGIN_H

#include "messenger.h"
#include <QHostAddress>

namespace Ui {
class Login;
}

class Login : public QWidget
{
    Q_OBJECT

public:
    explicit Login(Client &client, QWidget *parent = nullptr);
    ~Login();

signals:
    void login(QString username, QString password);
    void connection(QHostAddress addr, quint16 port);

private:
    Client &mClient;
    Ui::Login *ui;

private slots:
    void tryLogin();
    void connectServer();
    void ackConnection();
    void ackLogin(bool valid);
};

#endif // LOGIN_H
