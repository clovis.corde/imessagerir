#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      mClient(new Client(parent))
{
    qDebug() << "New Main Window";

    Login *l = new Login(mClient, this);
    setCentralWidget(l);
    connect(l, SIGNAL(login(QString,QString)), &mClient, SLOT(msgLogin(QString,QString)));
    connect(l, SIGNAL(connection(QHostAddress,quint16)), &mClient, SLOT(msgConnection(QHostAddress,quint16)));
    connect(&mClient, SIGNAL(emitSwitch(QString)), this, SLOT(switchScreenConv(QString)));
}

void MainWindow::switchScreenConv(QString username)
{
    setWindowTitle(username);
    setCentralWidget(new Messenger(mClient, this));
}
