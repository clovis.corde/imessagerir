#include "client.h"

Client::Client(QObject *parent): QObject(parent)
{
    mSocket = new QTcpSocket(this);
    connect(mSocket, SIGNAL(connected()), this, SLOT(ackConnection()));

    //connect(mSocket, SIGNAL(disconnected()), this, SLOT(logout()));
    //connect(mSocket, SIGNAL(readyRead()), this, SLOT(tryLogin()));
    //connect(mSocket, SIGNAL(errorOCcured()), this, SLOT(emitSocketError()));

}

void Client::login(QString username, QString password)
{
    QJsonObject jsonObj;
    // We prepare the package to send
    jsonObj["action"] = "login";
    jsonObj["login"] = username ;
    jsonObj["password"] = password;
    QByteArray package;
    package = QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);

    // We send the package
    mSocket->write(package);
    qDebug() << "username" << username;
    qDebug() << "Send Login Request";
}

void Client::ackConnection()
{
    emit emitConnected();
}

QString Client::username()
{
   return user;
}

void Client::sendMessage(QString msg)
{
    QJsonObject jsonObj;
    // We prepare the package to send
    jsonObj["action"] = "send";
    //jsonObj["target"] = username ;
    jsonObj["content"] = msg;
    QByteArray package;
    package = QJsonDocument(jsonObj).toJson(QJsonDocument::Compact);

    // We send the package
    mSocket->write(package);
    qDebug() << "Send Message Request";
}


void Client::dataReceived()
{

    /* Read the package */
    QString jsonReceived = mSocket->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(jsonReceived.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();

    QString action = jsonObject["action"].toString();

     if (action == "receive"){
        QString from = jsonObject["from"].toString();
        QString content = jsonObject["from"].toString();
        QString datetime = jsonObject["from"].toString();
        QString messageReceived = content + "\n";

        emit appendText(messageReceived);
        }
     else if (action == "answerlogin"){
         QString state = jsonObject["state"].toString();
         if (state == "ok" ){
             //emit logOK();
         }
         else if (state != "ok" ){
             return;
         }
     }
     else
         qDebug() <<("!!!   probleme avec le serveur    !!!");
         return;
    }



void Client::msgConnection(QHostAddress addr, quint16 port)
{
    qDebug() << "Try to connect";
    mSocket->connectToHost(addr, port);
}

void Client::sendLogin()
{
    emit emitLogin();
}

void Client::msgLogin(QString username, QString password)
{
    login(username,password);
    emit emitSwitch(username);
}
