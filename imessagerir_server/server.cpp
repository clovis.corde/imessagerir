#include "server.h"

Server::Server() {
    // Création et disposition des widgets de la fenêtre
    serverState = new QLabel;

    pbQuit = new QPushButton(tr("Quit"));
    connect(pbQuit, SIGNAL(clicked()), qApp, SLOT(quit()));

    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(serverState);
    layout->addWidget(pbQuit);
    setLayout(layout);
    setWindowTitle(tr("iMessagerir"));

    // Gestion du server
    server = new QTcpServer(this);

    // Démarrage du server sur toutes les IP disponibles et sur le port 50585
    if ( !server->listen(QHostAddress::Any, 50885) )    {
        // Si le server n'a pas été démarré correctement
        serverState->setText(tr("Le server n'a pas pu être démarré :<br />") + server->errorString());
    } else {
        // Si le server a été démarré correctement
        serverState->setText(tr("Server started on port <strong>") + QString::number(server->serverPort()));
        connect(server, SIGNAL(newConnection()), this, SLOT(newConnection()));
    }
    tailleMessage = 0;
}

void Server::newConnection() {
    sendAll(tr("<em>Un nouveau client vient de se connecter</em>"));
    qDebug("New Connexion");
    QTcpSocket *connection = server->nextPendingConnection();
    connect(connection, SIGNAL(disconnected()), this, SLOT(sockDisconnected()));
    connect(connection,SIGNAL(readyRead()),this,SLOT(clientTalks()));
    mListeSocks << connection;
}

void Server::sockDisconnected()
{
    QTcpSocket * sock = (QTcpSocket*)sender();
    mListeSocks.removeAll(sock);
    delete sock;
}

void Server::clientTalks()
{
    QTcpSocket * sock = (QTcpSocket*)sender();
    qDebug() << sock->readAll();
    sock->write("OK");
}

void Server::clientDeconnection() {
    sendAll(tr("<em>Un client vient de se déconnecter</em>"));

    // On détermine quel client se déconnecte
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());
    if ( socket == 0 )
        // Si on n'a pas trouvé le client à l'origine du signal, on arrête la méthode
        return;
    clients.removeOne(socket);
    socket->deleteLater();
}

void Server::dataReceived() {
    // 1 : on reçoit un paquet (ou un sous-paquet) d'un des clients
    // On détermine quel client envoie le message (recherche du QTcpSocket du client)
    QTcpSocket *socket = qobject_cast<QTcpSocket *>(sender());

    // Si on n'a pas trouvé le client à l'origine du signal, on arrête la méthode
    if ( socket == 0 )
        return;

    // Si tout va bien, on continue : on récupère le message
    QDataStream in(socket);

    // Si on ne connaît pas encore la taille du message, on essaie de la récupérer
    if ( tailleMessage == 0 ) {
        // On n'a pas reçu la taille du message en entier
        if ( socket->bytesAvailable() < (int)sizeof(quint16) )
            return;

        // Si on a reçu la taille du message en entier, on la récupère
        in >> tailleMessage;
    }

    // Si on connaît la taille du message, on vérifie si on a reçu le message en entier
    if ( socket->bytesAvailable() < tailleMessage )
        // Si on n'a pas encore tout reçu, on arrête la méthode
        return;

    // Si ces lignes s'exécutent, c'est qu'on a reçu tout le message : on le récupére !
    QString message;
    in >> message;

    // 2 : on renvoie le message à tous les clients
    sendAll(message);

    // 3 : remise de la taille du message à 0 pour la réception des futurs messages
    tailleMessage = 0;
}

void Server::sendAll(const QString &message) {
    // Préparation du paquet
    QByteArray paquet;
    QDataStream out(&paquet, QIODevice::WriteOnly);

    // On écrit 0 au début du paquet pour réserver la place pour écrire la taille
    out << (quint16) 0;
    out << message; // On ajoute le message à la suite
    out.device()->seek(0); // On se replace au début du paquet

    // On écrase le 0 qu'on avait réservé par la longueur du message
    out << (quint16) (paquet.size() - sizeof(quint16));

    // Envoi du paquet préparé à tous les clients connectés au server
    for (int i = 0; i < clients.size(); i++) {
        clients[i]->write(paquet);
    }
}
