#ifndef SERVER_H
#define SERVER_H

#include <QApplication>
#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QLabel>
#include <QPushButton>
#include <QVBoxLayout>
#include <QWidget>
#include <QtNetwork>


class Server : public QWidget {
    Q_OBJECT

public:
    Server();
    void sendAll(const QString &message);

private slots:
    void newConnection();
    void sockDisconnected();
    void clientTalks();
    void clientDeconnection();
    void dataReceived();

private:
    QList<QTcpSocket*> mListeSocks;
    QLabel *serverState;
    QPushButton *pbQuit;
    QTcpServer *server;
    QList<QTcpSocket *> clients;
    quint16 tailleMessage;
};

#endif // server_H
